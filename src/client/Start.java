package client;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import util.SessionFactoryUtil;
import entities.Author;
import entities.Book;

public class Start {
	public static void main(String[] args) {
		Session session = SessionFactoryUtil.getSessionFactory()
				.getCurrentSession();
		Transaction tx = session.beginTransaction();

		// Auto generated id
		Author goethe = new Author("Jonny", "von Goethe");
		session.save(goethe);
		System.out.println(goethe.getPseudonym() + "got id " + goethe.getPersonId());

		// Queries
		Book book1 = new Book();
		book1.setIsbn(123);
		book1.setTitle("Faust");
		session.save(book1);

		Book book2 = new Book();
		book2.setIsbn(345);
		book2.setTitle("Goetz vor Berlichingen");
		session.save(book2);

		Book book3 = new Book();
		book3.setIsbn(678);
		book3.setTitle("Faust Teil 2");
		session.save(book3);

		// Find book by primary key
		Book foundBook = (Book) session.load(Book.class, new Integer(345));
		System.out.println("Buch mit isbn 123: " + foundBook.getTitle());

		// Find book via query attributes
		Query query = session
				.createQuery("select k from Book k where k.title like 'Faus%'");
		List<Book> books = query.list();
		for (Book book : books) {
			System.out.println("We found a book " + book.getTitle()
					+ " with isbn " + book.getIsbn());
		}

		// 1:n associations
		book1.setAuthor(goethe);
		goethe.addBook(book1);
		book2.setAuthor(goethe);
		goethe.addBook(book2);
		session.save(goethe);

		Author anAuthor = (Author) session.load(Author.class, new Integer(1));
		for (Book book : anAuthor.getBooks()) {
			System.out.println("The author '" + book.getAuthor().getName()
					+ "' wrote the book " + book.getTitle() + " with isbn "
					+ book.getIsbn());
		}

		tx.commit();
		System.out.println("Fertig!");
		System.exit(0);
	}
}
