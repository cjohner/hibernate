package entities;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
@DiscriminatorValue("Angestellter")
public class Author extends Person {
	@Column(name="SecretName")
	private String pseudonym;

	@OneToMany(mappedBy = "author", cascade = CascadeType.ALL) //Cascade optional
	@OrderBy("isbn asc")
	private Collection<Book> books;

	public Author(String pseudonym, String name) {
		super(name);
		this.pseudonym = pseudonym;
		books = new HashSet<Book>();
	}
	
	public void addBook(Book b) {
		books.add(b);
	}

	public Collection<Book> getBooks() {
		return books;
	}

	public void setBooks(Collection<Book> books) {
		this.books = books;
	}

	public String getPseudonym() {
		return pseudonym;
	}

	public void setPseudonym(String pseudonym) {
		this.pseudonym = pseudonym;
	}
	
	
}
